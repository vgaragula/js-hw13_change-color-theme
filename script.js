
const btn = document.querySelector(".btn-toggle");
btn.addEventListener("click", function (event) {
event.preventDefault()
if(localStorage.getItem('theme')==='dark'){
  localStorage.removeItem('theme')
} else {
  localStorage.setItem('theme','dark')
}
addDarkClassToBody()
 
});
function addDarkClassToBody() {
  if(localStorage.getItem('theme')==='dark') {
    document.body.classList.add("dark");
  } else {
    document.body.classList.remove("dark");
  }
}
addDarkClassToBody()



